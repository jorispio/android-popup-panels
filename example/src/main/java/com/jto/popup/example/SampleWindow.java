package com.jto.popup.example;

import com.jto.android.popupinfowindow.popupwindow.PopupPanelsWindow;

public class SampleWindow extends PopupPanelsWindow {
    SampleWindow(final MainActivity activity) {
        super(activity);
        addPanel(new SamplePanel(activity).setText("First Panel"));
    }
}
