package com.jto.popup.example;

import android.graphics.Color;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.jto.android.popupinfowindow.popupwindow.AbstractPopupWindowPanel;

public class SamplePanel extends AbstractPopupWindowPanel {
    private final MainActivity mContext;
    private int color;
    private String text;

    public SamplePanel(MainActivity activity) {
        super(activity);
        this.mContext = activity;
        setLayout(R.layout.layout_panel);
        this.color = Color.BLACK;
        this.text = "";
    }


    public SamplePanel setText(String text) {
        this.text = text;
        return this;
    }

    public SamplePanel setColor(int color) {
        this.color = color;
        return this;
    }

    @Override
    protected View onLayout(@NonNull View view) {
        final LinearLayout bandColor = view.findViewById(R.id.bandColor);

        if (bandColor != null) {
            bandColor.setBackgroundColor(color);
        }

        final TextView txtTitle = view.findViewById(R.id.txtTitle);
        if (txtTitle != null) {
            txtTitle.setText(text);
        }

        return view;
    }
}
