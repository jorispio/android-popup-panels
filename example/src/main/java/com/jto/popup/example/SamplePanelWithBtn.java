package com.jto.popup.example;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.jto.android.popupinfowindow.popupwindow.AbstractPopupWindowPanel;

public class SamplePanelWithBtn extends AbstractPopupWindowPanel {
    private String text;
    public SamplePanelWithBtn(Context context) {
        super(context);
        setLayout(R.layout.layout_panel_with_btn);
        text = "";
    }


    public SamplePanelWithBtn setText(String text) {
        this.text = text;
        return this;
    }

    @Override
    protected View onLayout(@NonNull View view) {
        final TextView txtTitle = view.findViewById(R.id.txtTitle);
        if (txtTitle != null) {
            txtTitle.setText(text);
        }

        Button btnPanel = view.findViewById(R.id.btnPanel);
        if (btnPanel != null) {
            btnPanel.setOnClickListener(v -> SamplePanelWithBtn.this.remove());
        }

        return view;
    }
}
