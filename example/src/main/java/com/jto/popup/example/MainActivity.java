package com.jto.popup.example;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.jto.android.popupinfowindow.popupwindow.PanelAnimation;

public class MainActivity extends AppCompatActivity {
    private SampleWindow sampleWindow;
    private View layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity);

        Button btnShowHide = findViewById(R.id.btnShowHide);
        if (btnShowHide != null) {
            btnShowHide.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    layout = findViewById(R.id.layout);
                    sampleWindow.setParentView(layout);
                    sampleWindow.show();
                }
            });
        }

        Button btnAddPanel1 = findViewById(R.id.btnAddPanel1);
        if (btnAddPanel1 != null) {
            btnAddPanel1.setOnClickListener(v -> {
                sampleWindow.addPanel(new SamplePanel(MainActivity.this)
                        .setColor(Color.parseColor("#64b5f6"))
                        .setText("Simple Panel")
                        .setBackgroundAlpha(0.5f));
            });
        }

        Button btnAddPanel2 = findViewById(R.id.btnAddPanel2);
        if (btnAddPanel2 != null) {
            btnAddPanel2.setOnClickListener(v -> {
                sampleWindow.addPanel(new SamplePanel(MainActivity.this)
                        .setText("Panel with Translation")
                        .setColor(Color.parseColor("#7e57c2"))
                        .setAnimation(PanelAnimation.TRANSLATION) // animation when showing/hiding the panel
                        .setBackgroundColor(Color.BLACK)
                        .setBackgroundAlpha(0.5f)
                        .setCornerRadius(5));
            });
        }

        Button btnAddPanel3 = findViewById(R.id.btnAddPanel3);
        if (btnAddPanel3 != null) {
            btnAddPanel3.setOnClickListener(v -> {
                sampleWindow.addPanel(new SamplePanelWithBtn(MainActivity.this)
                        .setText("Panel with Button")
                        .setAnimation(PanelAnimation.FOLD) // animation when showing/hiding the panel
                        .setBackgroundAlpha(0.5f));
            });
        }

        Button btnAddTemporaryPanel = findViewById(R.id.btnAddTemporaryPanel);
        if (btnAddTemporaryPanel != null) {
            btnAddTemporaryPanel.setOnClickListener(v -> {
                sampleWindow.addPanel(new SamplePanel(MainActivity.this)
                        .setText("Panel with timeout")
                        .setColor(Color.parseColor("#607d8b"))
                        .setAnimation(PanelAnimation.LINEAR_EXPAND) // animation when showing/hiding the panel
                        .setTimeOut(5000));
            });
        }

        Button btnClear = findViewById(R.id.btnClear);
        if (btnClear != null) {
            btnClear.setOnClickListener(v -> sampleWindow.removeAllPanels());
        }

        layout = findViewById(R.id.layout);
        sampleWindow = (SampleWindow) new SampleWindow(this)
                .setWidth(800)
                .setMarginBetweenPanels(5)
                .setAnchorPoint(100, 10);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (sampleWindow != null) {
            sampleWindow.dismiss();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (sampleWindow != null) {
            //sampleWindow.onResume();
        }

        //sampleWindow.show();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (sampleWindow != null) {
            //sampleWindow.onPause();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (sampleWindow != null) {
            //sampleWindow.onDestroy();
        }
    }
}