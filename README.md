[![License](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)
[![](https://jitpack.io/v/com.gitlab.jorispio/android-popup-panels.svg)](https://jitpack.io/#com.gitlab.jorispio/android-popup-panels)

# Android Popup Panels
Android Popup Panels is an extension of LinearLayout/PopupWindow to easily add panels.
The popup window is a floating container that can be placed on top of the current activity or fragment. 
A Panel is a sub-layout that can be added to its parent layout.
 

<img src="screenshots/snapshot.gif"/>


## Instructions
### Common and generic setup
Simply extends your window class as:
```
    class SampleWindow extends PopupPanelsWindowManager {
        SampleWindow(final MainActivity activity) {
            super(activity);        
        }
    }
```

And create panels extending AbstractPopupWindowPanel:
```
    class SamplePanel extends AbstractPopupWindowPanel {
    
        public SamplePanel(Context context) {
            super(context);
            setLayout(R.layout.layout_panel1);
        }
        
        @Override
        public AbstractPopupWindowPanel setLayout(View view) {
            // setup your view elements
            return view;
        }
    }
```
        
Instantiate your panel:
```        
    panel = new SamplePanel()
            .setTimeOut(1000) // panel time out, in milliseconds
            .setAnimation() // animation when showing/hiding the panel
            .setPadding(5)
            .setBackground()
            .setBackgroundColor()
            .setBackgroundAlpha();
```

Finally, instantiate the window and add initial panels,
```                
    window = new SampleWindow()
        .setGravity(Gravity.TOP | Gravity.START)
        .setPadding(5)
        .addPanel(panel)
        .show();
```

You can add panel dynamically with,
```                
    window.addPanel(panel);
```


## Download and Installation

Declare the repository (if not already done):
```
repositories {
    maven {
        url "https://jitpack.io"
    }
}
```

Then add the dependency:
```
dependencies {
    implementation 'com.gitlab.jorispio:android-popup-panels:1.0.0@aar'
}
```



## License
```
The MIT License

Copyright (c) 2018-2019 Joris OLYMPIO

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
```