package com.jto.android.popupinfowindow.popupwindow;

public enum PanelAnimation {
    NONE,
    TRANSLATION,
    LINEAR_EXPAND,
    BOUNCE,
    FOLD
}
