package com.jto.android.popupinfowindow.popupwindow.animations;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.Transformation;

public class LinearHeightExpandAnimation extends Animation {
    private View mView;
    private int mStartHeight, mTargetHeight;

    public LinearHeightExpandAnimation(View view, int startHeight, int targetHeight) {
        mView = view;
        mStartHeight = startHeight;
        mTargetHeight = targetHeight;
    }

    @Override
    public void initialize(int width, int height, int parentWidth, int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
    }

    public LinearHeightExpandAnimation withInterpolator(Interpolator interpolator) {
        if (interpolator != null) {
            this.setInterpolator(interpolator);
        }
        return this;
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        mView.getLayoutParams().height = (int) (mStartHeight + (mTargetHeight * interpolatedTime));
        mView.requestLayout();
    }
}