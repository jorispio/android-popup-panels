package com.jto.android.popupinfowindow.popupwindow;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jto.android.popupinfowindow.R;

public class PopupWindow implements View.OnClickListener, android.widget.PopupWindow.OnDismissListener {
    private static final String TAG = "PANELS";
    private static final int OFFSET_X = 10;
    private static final int OFFSET_Y = 10;

    private View mParentView;
    private String title;
    @DrawableRes private int mIcon;
    private int backgroundColor;
    private float cornerRadius;
    private boolean focusable;
    private boolean cancelable;
    private boolean isLaidOutInScreen;
    private boolean clippingEnabled;
    private boolean overlapAnchor;
    private int mWidth;
    private int xoff;
    private int yoff;
    private int elevation;
    private int customViewRes;
    private int gravity;

    @Nullable
    private View mView;
    private LayoutInflater layoutInflater;
    private View.OnClickListener mOnClickListener;
    private View mPopupView;
    private android.widget.PopupWindow mPopupWindow;
    private DisplayMetrics dm = new DisplayMetrics();
    private boolean isLandscape;
    Context context;

    public PopupWindow(Context context, @NonNull String title) {
        this(context, null, title);
    }

    /** Constructor */
    public PopupWindow(Context context, @Nullable View anchorView, @NonNull String title) {
        final Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (windowManager != null) {
            windowManager.getDefaultDisplay().getMetrics(dm);
        }

        this.context = context;

        final int screenOrientation = display.getRotation();
        this.isLandscape = (screenOrientation == Surface.ROTATION_90) || (screenOrientation == Surface.ROTATION_270);
        this.mView = anchorView;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.title = title;

        this.customViewRes = R.layout.popupwindow;
        this.backgroundColor = Color.BLACK;
        this.cornerRadius = 0f;
        this.mWidth = ViewGroup.LayoutParams.WRAP_CONTENT;
        this.xoff = OFFSET_X;
        this.yoff = OFFSET_Y;
        this.gravity = Gravity.TOP | Gravity.START;
        this.elevation = 4;
        this.focusable = true;
        this.cancelable = false;
        this.isLaidOutInScreen = false;
        this.clippingEnabled = false;
        this.overlapAnchor = false;
    }

    public PopupWindow setWidth(int width) {
        this.mWidth = width;
        return this;
    }

    public PopupWindow setIsLaidOutInScreen(boolean isLaidOutInScreen) {
        this.isLaidOutInScreen = isLaidOutInScreen;
        return this;
    }

    public PopupWindow setClippingEnabled(boolean clippingEnabled) {
        this.clippingEnabled = clippingEnabled;
        return this;
    }

    public PopupWindow setGravity(int gravity) {
        this.gravity = gravity;
        return this;
    }

    public PopupWindow setOverlapAnchor (boolean overlapAnchor) {
        this.overlapAnchor = overlapAnchor;
        return this;
    }

    /** */
    void inflate(ViewGroup root) {
        mPopupView = layoutInflater.inflate(customViewRes, root, false);

        final TextView titleBar = mPopupView.findViewById(R.id.title);
        if (titleBar != null) {
            titleBar.setText(title);
        }

        final ImageView titleIcon = mPopupView.findViewById(R.id.icon);
        if (titleIcon != null) {
            titleIcon.setImageResource(mIcon);
        }

        final Button closeButton = mPopupView.findViewById(R.id.btn_menu_close);
        if (closeButton != null) {
            closeButton.setOnClickListener(v -> mPopupWindow.dismiss());
        }

        final LinearLayout mainLayout = mPopupView.findViewById(R.id.window_layout_content);
        // set click listener for all children of the layout
        for(int index = 0; index< mainLayout.getChildCount(); ++index) {
            final View nextChild = mainLayout.getChildAt(index);
            setEvent(nextChild);
        }

        // remove old window
        if (mPopupWindow != null) {
            mPopupWindow.dismiss();
            mPopupWindow = null;
        }

        // background and shape
        final GradientDrawable backgroundDrawable = new GradientDrawable();
        backgroundDrawable.setColor(backgroundColor);
        backgroundDrawable.setCornerRadius(cornerRadius);

        // create popup window
        mPopupWindow = new android.widget.PopupWindow(
                mPopupView,
                mWidth,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        mPopupWindow.setFocusable(focusable);
        mPopupWindow.setBackgroundDrawable(backgroundDrawable); //new BitmapDrawable());
        mPopupWindow.setOutsideTouchable(false);
        mPopupWindow.setClippingEnabled(clippingEnabled);
        mPopupWindow.setElevation(elevation);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            mPopupWindow.setIsLaidOutInScreen(isLaidOutInScreen);
        }
        mPopupWindow.setOverlapAnchor(overlapAnchor);
        mPopupWindow.setOnDismissListener(this);
        mPopupWindow.getContentView().getViewTreeObserver().addOnGlobalLayoutListener(mLayoutListener);
    }

    public android.widget.PopupWindow getPopupWindow() {
        return mPopupWindow;
    }

    public View getPopupView() {
        return mPopupView;
    }

    public PopupWindow setTitleIcon(@DrawableRes int icon) {
        this.mIcon = icon;
        return this;
    }

    /**
     * Changes this background color.
     * @param argb The color used to fill the window
     * @return this
     */
    public PopupWindow setBackgroundBottomColor(@ColorInt int argb) {
        this.backgroundColor = argb;
        return this;
    }

    /**
     * Changes this background color.
     * @param resid color ressource id to fill the window
     * @return this
     */
    public PopupWindow setBackgroundBottomColor(Context context, int resid) {
        this.backgroundColor = context.getColor(resid);
        return this;
    }

    public PopupWindow setCornerRadius(float radius) {
        this.cornerRadius = radius;
        return this;
    }

    public PopupWindow setCanceledOnTouchOutside(boolean cancelable) {
        this.cancelable = cancelable;
        return this;
    }

    public PopupWindow setFocusable(boolean focusable) {
        this.focusable = focusable;
        return this;
    }

    public PopupWindow setCustomView(int customView) {
        this.customViewRes = customView;
        return this;
    }

    private void setEvent(@NonNull final View nextChild) {
        if ((nextChild instanceof LinearLayout) || (nextChild instanceof RelativeLayout)) {
            final ViewGroup viewGroup = ((ViewGroup)nextChild);
            final int n = viewGroup.getChildCount();
            for (int index=0; index < n; ++index) {
                // recursive
                setEvent(viewGroup.getChildAt(index));
            }
        }
    }

    /**
     * Display the popup window anchored to the bottom-left corner of the parent (anchor view).
     * @param parent anchor view
     * @return this
     */
    public PopupWindow setParentView(View parent) {
        this.mParentView = parent;
        return this;
    }

    /**
     * Display the popup window anchored to the bottom-left
     * corner of the anchor view offset by the specified x and y coordinates.
     *
     * @param xoff  horizontal offset from the anchor in pixels
     * @param yoff  vertical offset from the anchor in pixels
     * @return this
     */
    public PopupWindow setAnchorPoint(int xoff, int yoff) {
        this.xoff = xoff;
        this.yoff = yoff;
        return this;
    }

    /**
     * Set window elevation
     * @param elevation elevation
     * @return this
     */
    public PopupWindow setElevation(int elevation) {
        this.elevation = elevation;
        return this;
    }

    private void showAtLocation(int xSystemUiOff, int ySystemUiOff) {
        if (mParentView != null) {
            //PointF rect = calcPopupLocation();
            RectF rect = PanelsUtils.calculeRectInWindow(mParentView);
            if (mPopupWindow.isShowing()) {
                mPopupWindow.setClippingEnabled(true);
                mPopupWindow.update(
                        (int) rect.left + xoff + xSystemUiOff,
                        (int) rect.top + yoff + ySystemUiOff,
                        -1, -1);
                Log.d(TAG, String.format("Updating panel at location (%d, %d)", (int) rect.left + xoff + xSystemUiOff, (int) rect.top + yoff + ySystemUiOff));
            } else {
                mPopupWindow.showAtLocation(mParentView, gravity,
                        (int) rect.left + xoff + xSystemUiOff,
                        (int) rect.top + yoff + ySystemUiOff);
                Log.d(TAG, String.format("Creating panel with anchor view %s at location (%d, %d)",
                        mParentView.toString(),
                        (int) rect.left + xoff + xSystemUiOff, (int) rect.top + yoff + ySystemUiOff));
            }
        } else {
            Log.e(TAG, "Anchor view is null");
        }
    }


    /** Show the popup menu, next to the button. The position depends on the display mode,
     * and the button position */
    public final void show() {
        if (mView == null) {
            if (mParentView == null) {
                Log.e(TAG, "Parent view is null. Cannot display Popup Window");
                return;
            }
            if (mPopupWindow != null) {
                showAtLocation(0, 0);

                View.OnSystemUiVisibilityChangeListener listener = new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        Log.d(TAG, "System UI visibility change: " + (visibility & View.SYSTEM_UI_FLAG_FULLSCREEN));
                        int statusBarHeight = PanelsUtils.getStatusBarHeight(context);
                        if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                            showAtLocation(0, statusBarHeight);
                        } else {
                            showAtLocation(0, -statusBarHeight);
                        }
                    }
                };
                mParentView.setOnSystemUiVisibilityChangeListener(listener);
            }
            return;
        }

        //Initialize the Point with x, and y positions
        final int[] location = new int[2];
        mView.getLocationOnScreen(location);
        final Point p = new Point();
        p.x = location[0] + OFFSET_X;
        p.y = location[1] + OFFSET_Y;
        if (isLandscape) {
            p.x += mView.getWidth();
            // correct y position to maximize popup window height
            if (p.y + mPopupView.getHeight() > dm.heightPixels) {
                p.y = dm.heightPixels - mPopupView.getHeight();
            }
        } else {
            p.y += mView.getHeight();
        }

        mPopupWindow.showAtLocation(mView, Gravity.NO_GRAVITY, p.x, p.y);
    }

    /**
     * Returns the visibility of this view
     *
     * @return True if this view is visible
     */
    public boolean isShowing() {
        if (mPopupView != null) {
            return mPopupView.isShown();
        }
        return false;
    }

    /**
     * Disposes of the popup window.
     * This method will have no effect is the window is not shown.
     */
    public void dismiss() {
        if (mPopupWindow != null) {
            mPopupWindow.dismiss();
        }
        if (mParentView != null) {
            mParentView.setOnSystemUiVisibilityChangeListener(null);
        }
    }

    @Override
    public void onDismiss() {
        if (mPopupWindow != null) {
            mPopupWindow.getContentView().getViewTreeObserver().removeOnGlobalLayoutListener(mLayoutListener);
        }
    }

    private PointF calcPopupLocation() {
        PointF location = new PointF();
        final RectF anchorRect = PanelsUtils.calculeRectInWindow(mView);
        final PointF anchorCenter = new PointF(anchorRect.centerX(), anchorRect.centerY());
        if (isLandscape) {
            location.x = anchorRect.right + OFFSET_X;
            location.y = anchorRect.top;
            // correct y position to maximize popup window height
            if (location.y + mPopupWindow.getContentView().getHeight() > 0) {
                location.y = anchorRect.top - mPopupWindow.getContentView().getHeight();
                location.y = Math.max(location.y, 0);
            }
        } else {
            location.x = anchorRect.left;
            location.y = anchorCenter.y  + OFFSET_Y;
        }
        return location;
    }

    public View getView(int id) {
        return mPopupView.findViewById(id);
    }

    public View addView(@NonNull View childView) {
        final View linearLayout = mPopupView.findViewById(R.id.window_layout_content);
        if (linearLayout != null) {
            ViewParent parent = childView.getParent();
            if (parent != null) {
                ((ViewGroup) parent).removeView(childView);
            }

            ((LinearLayout) linearLayout).addView(childView);
            return childView;
        }
        return null;
    }

    public void setOnClickListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }

    @Override
    public void onClick(View v) {
        if (mOnClickListener != null) {
            mOnClickListener.onClick(v);
        }
    }

    private final ViewTreeObserver.OnGlobalLayoutListener mLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            final android.widget.PopupWindow popup = mPopupWindow;
            if (isLandscape && (mView != null)) {
                PointF location = calcPopupLocation();
                popup.setClippingEnabled(true);
                popup.update((int) location.x, (int) location.y, popup.getWidth(), popup.getHeight());
            }

            popup.getContentView().requestLayout();
        }
    };
}
