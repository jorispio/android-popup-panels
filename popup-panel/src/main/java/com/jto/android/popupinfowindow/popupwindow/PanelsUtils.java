package com.jto.android.popupinfowindow.popupwindow;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.RectF;
import android.view.View;

import androidx.annotation.NonNull;

class PanelsUtils {
    private static final String TAG = "UTILS";

    static int getStatusBarHeight(Context mParentView) {
        int height = 0;
        Resources myResources = mParentView.getResources();
        int idStatusBarHeight = myResources.getIdentifier( "status_bar_height", "dimen", "android");
        if (idStatusBarHeight > 0) {
            height = myResources.getDimensionPixelSize(idStatusBarHeight);
        }
        return height;
    }

    static RectF calculeRectInWindow(@NonNull View view) {
        int[] location = new int[2];
        view.getLocationInWindow(location);
        return new RectF(location[0], location[1], location[0] + view.getMeasuredWidth(), location[1] + view.getMeasuredHeight());
    }

    static RectF calculeRectInScreen(@NonNull View view) {
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        return new RectF(location[0], location[1], location[0] + view.getMeasuredWidth(), location[1] + view.getMeasuredHeight());
    }
}
