package com.jto.android.popupinfowindow.popupwindow;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;

import androidx.annotation.CallSuper;
import androidx.annotation.ColorInt;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jto.android.popupinfowindow.R;
import com.jto.android.popupinfowindow.popupwindow.animations.BounceAnimation;
import com.jto.android.popupinfowindow.popupwindow.animations.LinearHeightExpandAnimation;
import com.jto.android.popupinfowindow.popupwindow.animations.UnfoldAnimation;

public abstract class AbstractPopupWindowPanel /*extends View*/ {
    @LayoutRes private int layout;
    private long mTimeOutDuration;
    private boolean isAdded;
    private boolean mShowing;
    @ColorInt private int mBackgroundColor;
    private float mBackgroundAlpha;
    private int mCornerRadius;
    @NonNull private PanelAnimation mPanelAnimation;
    private long mPanelAnimationDuration;

    private PopupPanelsWindow popupPanelsWindowManager;
    private android.widget.PopupWindow.OnDismissListener dismissListener;

    public AbstractPopupWindowPanel(Context context) {
        this(context, null);
    }

    public AbstractPopupWindowPanel(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AbstractPopupWindowPanel(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public AbstractPopupWindowPanel(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        //super(context, attrs, defStyleAttr, defStyleRes);

        this.layout = R.layout.popuppanel;
        this.mTimeOutDuration = 0L;
        this.mPanelAnimation = PanelAnimation.NONE;
        this.mPanelAnimationDuration = 500;
        this.mBackgroundAlpha = 0f;
        this.mCornerRadius = 5;
        this.isAdded = false;
        this.mShowing = false;
    }

    Animation buildAnimation(@NonNull View view) {
        int height = view.getLayoutParams().height;

        if (mPanelAnimation == PanelAnimation.TRANSLATION) {
            TranslateAnimation animation = new TranslateAnimation(
                    0,
                    0,
                    -100,
                    0);
            animation.setInterpolator(new AccelerateDecelerateInterpolator());
            animation.setDuration(mPanelAnimationDuration);
            animation.setFillAfter(true);
            return animation;
        }

        if (mPanelAnimation == PanelAnimation.LINEAR_EXPAND) {
            LinearHeightExpandAnimation animation = new LinearHeightExpandAnimation(view, 0, height);
            animation.withInterpolator(new LinearInterpolator())
                     .setDuration(mPanelAnimationDuration);
            animation.setFillAfter(true);
            view.getLayoutParams().height = 0;
            return animation;
        }

        if (mPanelAnimation == PanelAnimation.BOUNCE) {
            BounceAnimation animation = new BounceAnimation(view, 0, height);
            animation.withInterpolator(new AccelerateDecelerateInterpolator())
                    .setDuration(mPanelAnimationDuration);
            animation.setFillAfter(true);
            return animation;
        }

        if (mPanelAnimation == PanelAnimation.FOLD) {
            UnfoldAnimation animation = new UnfoldAnimation(view, height, mPanelAnimationDuration);
            animation.setFillAfter(true);
            return animation;
        }

        return null;
    }

    public AbstractPopupWindowPanel setLayout(@LayoutRes int layout) {
        this.layout = layout;
        return this;
    }

    abstract protected View onLayout(@NonNull View view);

    protected View buildLayout(@NonNull Context context, @NonNull ViewGroup root) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(layout, root, false);
        onLayout(view);
        return view;
    }

    public AbstractPopupWindowPanel setOnDismissListener(android.widget.PopupWindow.OnDismissListener dismissListener) {
        this.dismissListener = dismissListener;
        return this;
    }

    public AbstractPopupWindowPanel setTimeOut(long delay) {
        this.mTimeOutDuration = delay;
        return this;
    }

    public PanelAnimation getAnimation() {
        return this.mPanelAnimation;
    }

    public final AbstractPopupWindowPanel setAnimation(PanelAnimation animation) {
        this.mPanelAnimation = animation;
        return this;
    }

    public long getAnimationDuration() {
        return mPanelAnimationDuration;
    }

    public AbstractPopupWindowPanel setAnimationDuration(long duration) {
        this.mPanelAnimationDuration = duration;
        return this;
    }

    public final AbstractPopupWindowPanel setBackgroundColor(@ColorInt int color) {
        this.mBackgroundColor = color;
        return this;
    }

    @ColorInt
    public int getBackgroundColor() {
        return this.mBackgroundColor;
    }

    public float getBackgroundAlpha() {
        return this.mBackgroundAlpha;
    }

    public final AbstractPopupWindowPanel setBackgroundAlpha(float transparent) {
        this.mBackgroundAlpha = transparent;
        return this;
    }

    public int getCornerRadius() {
        return mCornerRadius;
    }

    public final AbstractPopupWindowPanel setCornerRadius(int radius) {
        this.mCornerRadius = radius;
        return this;
    }

    /**
     * Return the panel height
     * @return panel height
     */
    public int getHeight() {
        return 0;
    }

    @CallSuper
    public void show(@NonNull PopupPanelsWindow popupPanelsWindowManager) {
        build(popupPanelsWindowManager);
        mShowing = true;
    }

    void dismiss() {
        if (mShowing) {
            if (dismissListener != null) {
                dismissListener.onDismiss();
            }
        }

        mShowing = false;
    }

    @CallSuper
    public void remove() {
        if (mShowing) {
            if (popupPanelsWindowManager != null) {
                popupPanelsWindowManager.removePanel(this);
            }

            if (dismissListener != null) {
                dismissListener.onDismiss();
            }
        }

        mShowing = false;
    }

    void setAdded(boolean value) {
        isAdded = value;
    }

    public boolean isAdded() {
        return isAdded;
    }

    public boolean isShowing() {
        return mShowing;
    }

    @CallSuper
    protected final void build(@NonNull PopupPanelsWindow popupPanelsWindowManager) {
        this.popupPanelsWindowManager = popupPanelsWindowManager;
        if (mTimeOutDuration > 0) {
            addClosingTimer(this, mTimeOutDuration);
        }
    }

    private void addClosingTimer(final AbstractPopupWindowPanel panel, long durationMilliSec) {
        final Handler handler  = new Handler();
        final Runnable runnable = () -> {
            if (panel.isShowing()) {
                panel.remove();
            }
        };

        panel.setOnDismissListener(() -> handler.removeCallbacks(runnable));
        handler.postDelayed(runnable, durationMilliSec);
    }
}
