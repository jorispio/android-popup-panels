package com.jto.android.popupinfowindow.popupwindow;

import android.app.Activity;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;

import com.jto.android.popupinfowindow.R;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.locks.ReentrantLock;

public class PopupPanelsWindow {
    private final static float POPUP_CORNER_RADIUS = 20f;

    private final Activity mActivity;
    private final PopupWindow popupWindow;

    private View mParentView;
    private int marginBetweenPanels;
    private int mWidth;
    private final ArrayList<AbstractPopupWindowPanel> mPanels;

    private ReentrantLock lock = new ReentrantLock();

    /** Constructor */
    public PopupPanelsWindow(@NonNull final Activity activity) {
        this(activity, null);
    }

    /** Constructor */
    public PopupPanelsWindow(@NonNull final Activity activity, View anchorView) {
        this.mActivity = activity;
        this.mPanels = new ArrayList<>();
        this.popupWindow = new PopupWindow(activity, anchorView, "");
        this.mWidth = ViewGroup.LayoutParams.WRAP_CONTENT;
        this.popupWindow.setCornerRadius(POPUP_CORNER_RADIUS)
                .setBackgroundBottomColor(activity, R.color.preference_background)
                .setCanceledOnTouchOutside(false)
                .setFocusable(false);
    }

    /**
     * Set the background color of the window. This color will appear between
     * panels.
     * @param color background color of the window
     * @return this
     */
    public PopupPanelsWindow setBackgroundColor(@ColorInt int color) {
        popupWindow.setBackgroundBottomColor(color);
        return this;
    }

    /** @return the popup window */
    public PopupWindow getPopupWindow() {
        return popupWindow;
    }

    /**
     * Set corner radius of the windox
     * @param radius corner radius of the window, px
     * @return this
     */
    public PopupPanelsWindow setCornerRadius(float radius) {
        popupWindow.setCornerRadius(radius);
        return this;
    }

    /**
     *
     * @param parentView parent view
     * @return this
     */
    public PopupPanelsWindow setParentView(@NonNull View parentView) {
        if (this.mParentView == parentView) return this;

        this.mParentView = parentView;
        if (popupWindow != null) {
            popupWindow.setParentView(parentView);
        }
        return this;
    }

    /**
     * Display the popup window anchored to the bottom-left
     * corner of the anchor view offset by the specified x and y coordinates.
     *
     * @param xoff  horizontal offset from the anchor in pixels
     * @param yoff  vertical offset from the anchor in pixels
     * @return this
     */
    public PopupPanelsWindow setAnchorPoint(int xoff, int yoff) {
        popupWindow.setAnchorPoint(xoff, yoff);
        return this;
    }

    public PopupPanelsWindow setMarginBetweenPanels(int margin) {
        this.marginBetweenPanels = margin;
        return this;
    }

    public PopupPanelsWindow setWidth(int width) {
        this.mWidth = width;
        return this;
    }

    /**
     * Add a panel
     * @param panel panel to add to the popup window
     */
    public synchronized void addPanel(@NonNull AbstractPopupWindowPanel panel) {
        lock.lock();
        mPanels.add(panel);
        panel.setAdded(true);
        lock.unlock();

        if (mParentView != null) {
            final LinearLayout linearLayout = popupWindow.getPopupView().findViewById(R.id.window_layout_content);

            View view = panel.buildLayout(mActivity, linearLayout);

            // set margin
            if (marginBetweenPanels > 0) {
                ViewGroup.LayoutParams params = view.getLayoutParams();
                if (params != null) {
                    if (params instanceof LinearLayout.LayoutParams) {
                        ((LinearLayout.LayoutParams)params).setMargins(0, marginBetweenPanels, 0, marginBetweenPanels);
                        view.setLayoutParams(params);
                    }
                }
            }

            linearLayout.addView(view);

            if (panel.getAnimation() != PanelAnimation.NONE) {
                view.startAnimation(panel.buildAnimation(view));
            }

            if (panel.getBackgroundColor() != 0) {
                GradientDrawable drawable = new GradientDrawable();
                drawable.setColor(panel.getBackgroundColor());
                if (panel.getCornerRadius() > 0) {
                    drawable.setCornerRadius(panel.getCornerRadius());
                }
                view.setBackground(drawable);
            }

            if (panel.getBackgroundAlpha() > 0) {
                view.setAlpha(panel.getBackgroundAlpha());
            }

            panel.show(this);
        }
    }

    public synchronized boolean removePanel(@NonNull Iterator<AbstractPopupWindowPanel> panel) {
        panel.remove();
        return true;
    }

    /**
     * Remove a panel
     * @param panel panel to remove from popup window
     * @return true if panel was removed from popup window, false otherwise
     */
    public synchronized boolean removePanel(@NonNull AbstractPopupWindowPanel panel) {
        lock.lock();
        int index = mPanels.indexOf(panel);
        if (index == -1) {
            lock.unlock();
            return false;
        }

        mPanels.remove(index);
        if (mParentView != null) {
            final LinearLayout linearLayout = popupWindow.getPopupView().findViewById(R.id.window_layout_content);
            if ((linearLayout != null) && (linearLayout.getChildAt(index) != null)) {
                linearLayout.removeViewAt(index);
            }
        }

        lock.unlock();
        return true;
    }

    /**
     * Remove all panels.
     */
    public synchronized void removeAllPanels() {
        lock.lock();
        mPanels.clear();
        if (mParentView != null) {
            final LinearLayout linearLayout = popupWindow.getPopupView().findViewById(R.id.window_layout_content);
            if (linearLayout != null) {
                linearLayout.removeAllViews();
            }
        }
        lock.unlock();
    }

    /**
     * Get the panel at index
     * @param index panel index. Must be < getPanelCount().
     * @return the selected panel
     * @see #getPanelCount()
     */
    public synchronized Object getPanel(int index) {
        if ((index >= 0) && (index < mPanels.size())) {
            return mPanels.get(index);
        }
        return null;
    }

    /**
     * Get the number of panels
     * @return the number of panels
     */
    public Object getPanelCount() {
        return mPanels.size();
    }

    /** Show the window with panels */
    public void show() {
        if (mParentView == null) {
            throw new RuntimeException("ParentView shall be set before calling show().");
        }

        if ((popupWindow.getPopupView() == null) && (!popupWindow.isShowing())){
            popupWindow.setWidth(mWidth)
                        .inflate((ViewGroup) mParentView);
        }

        // add declared panels
        View popupView = popupWindow.getPopupView();
        if (popupView != null) {
            lock.lock();
            final LinearLayout linearLayout = popupWindow.getPopupView().findViewById(R.id.window_layout_content);
            for (AbstractPopupWindowPanel panel : mPanels) {
                if (!panel.isShowing()) {
                    View view = panel.buildLayout(mActivity, linearLayout);
                    linearLayout.addView(view);
                    panel.show(this);
                    panel.setAdded(true);
                }
            }
            lock.unlock();
        }

        popupWindow.show();
    }

    /**
     * Disposes of the popup window.
     * This method will have no effect is the window is not shown.
     */
    public synchronized void dismiss() {
        if (!popupWindow.isShowing()) {
            return;
        }

        lock.lock();
        for (Iterator<AbstractPopupWindowPanel> iterator = mPanels.iterator(); iterator.hasNext();) {
            AbstractPopupWindowPanel panel = iterator.next();
            if (panel.isAdded()) {
                panel.dismiss();
                removePanel(iterator);
            }
        }
        lock.unlock();

        popupWindow.dismiss();
    }

    public synchronized int getHeight() {
        int height = 0;
        for (AbstractPopupWindowPanel panel : mPanels) {
            height += panel.getHeight() + marginBetweenPanels;
        }
        return height;
    }
}
