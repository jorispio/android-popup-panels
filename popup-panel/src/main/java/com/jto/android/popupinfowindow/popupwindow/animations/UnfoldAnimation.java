package com.jto.android.popupinfowindow.popupwindow.animations;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.Transformation;

public class UnfoldAnimation extends Animation {
    private View mView;
    private final int mCameraZ;
    private float mFromAngleDegrees;
    private float mToAngleDegrees;
    private float mCenterX;
    private float mCenterY;
    private Camera mCamera;

    public UnfoldAnimation(View view, int cameraZ, long duration) {
        this.mView = view;
        this.setFillAfter(true);
        this.setDuration(duration);
        this.mCameraZ = cameraZ;
        this.mCamera = new Camera();
    }

    @Override
    public void initialize(int width, int height, int parentWidth, int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
        // place the camera on z, away from screen
        this.mCamera.setLocation(0, 0, -mCameraZ);
        this.mCenterX = width / 2;
        this.mCenterY = 0;
        this.mFromAngleDegrees = 90;
        this.mToAngleDegrees = 0;
    }

    public UnfoldAnimation withInterpolator(Interpolator interpolator) {
        if (interpolator != null) {
            this.setInterpolator(interpolator);
        }
        return this;
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        // rotation of the panel
        final float degrees = mFromAngleDegrees + ((mToAngleDegrees - mFromAngleDegrees) * interpolatedTime);

        // rotate the panel around X, so the panel is initially perpendicular to screen
        final Matrix matrix = t.getMatrix();
        mCamera.save();
        mCamera.rotateX(degrees);
        mCamera.getMatrix(matrix);
        mCamera.restore();

        // translate
        matrix.preTranslate(-mCenterX, -mCenterY);
        // translate back
        matrix.postTranslate(mCenterX, mCenterY);
    }
}